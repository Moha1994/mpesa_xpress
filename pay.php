<?php
$mysqli = new mysqli("localhost", "root", "", "mtugo");

/* check connection:-object oriented */
	$query="SELECT*FROM order_details";
		if ($result = $mysqli->query($query)){
		while ($row=mysqli_fetch_assoc($result)) {
			$phoneNumber=$row['PhoneNumber'];
			$Amount=$row['Amount'];
	}
   mysqli_free_result($result);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Pay for Meals</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<div style="width:250px; margin:50px auto;" >
	<form method="post" action="pay1.php">
		<?php
			if(isset($successMg)){
				?><div class="alert alert-success"><span class="glyphicon glyphicon-info-sign"><?php echo $successMg;?></span>
				</div>
			<?php
			}
			?>
		<div class="form-group">
			<label for="phoneNumber">PhoneNumber:</label>
			<input type="number" name="phoneNumber" value="<?php echo $phoneNumber;?>" class="form-control">
		</div>
		<div>
			<label for="Amount">Amount:</label>
			<input type="currency" name="Amount" value="<?php echo  $Amount;?>"class="form-control">
		</div>
	</hr><br><br>
		<div>
			<div class="text-center">
				<input type="submit" name="btn-submit" class="btn  btn-success" >
			</div>
			
		</div>
	</form>
</div>
</body>
</html>
