<?php
	include_once('db_config_tl.php');
		  $error=false;
		  
    if (isset($_POST['btn-order'])) {
    	//sql injection protection
       $id=$_POST['id'];
       $id=strip_tags($id);
       $id=htmlspecialchars($id);

    	 $Meal_desc=$_POST['Meal_desc'];
         $Meal_desc=strip_tags($Meal_desc);
         $Meal_desc=htmlspecialchars($Meal_desc);

        /*$amount=$_POST['amount'];
        $amount=strip_tags($amount);
        $amount=htmlspecialchars($amount);*/
        //validation checks

        if (empty($id)) {
               $error=true;
               $errorOrder='Please input  Id';
            }
            if (empty($Meal_desc)) {
                $error=true;
                $errorMeal_desc='Please enter Correct Meal_description';
            }// insert query
             if (!$error) {
           $order="INSERT INTO orders(id,Meal_desc)
            VALUES('$id','$Meal_desc')";
            if($conn->query($order)){
            	 $successMg='Order Successful"</br>"<a href="pay.php">Click to Pay</a>';   
            }
            else{
            	$conn->close();
            }
           
         }
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Order Form</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div style="width:400px; margin:50px auto;" >
		<form method="Post" action="meal_order.php">
		</hr>
		<!--Success Popup Message-->
		<?php
			if(isset($successMg)){
				?><div class="alert alert-success"><span class="glyphicon glyphicon-info-sign"><?php echo $successMg;?></span>
				</div>
			<?php
			}
			?>
			<fieldset>
				<legend class="badge badge-success active" ><h4><span class="badge">Order Details</span></h4></legend>
				<div class="form-group">
					<label for="Id">ID:</label>
				<input type="text" name="id" class="form-control">
				<span class="text-danger"><?php if (isset($errorOrder)) echo $errorOrder ?></span>
				</div>
				<div class="form-group">
					<label for="Meal_desc">Meal_description:</label>
				<textarea name="Meal_desc" class="form-control" cols="5" rows="5"></textarea>
				<span class="text-danger"><?php if (isset($errorMeal_desc)) echo $errorMeal_desc ?></span>
				</div>
				<div>
					<center><input type="submit" value="Order Meals"  name="btn-order" class="btn btn-success"></center>
				</div>
			</fieldset>
		</form>
	</div>

</body>
</html>
